import requests # pour envoyer des requêtes HTTP
from pyfiglet import Figlet # pour les fonts
import folium # pour la map


def get_info_by_ip(ip='127.0.0.1'):
    try:
        response = requests.get(url=f'http://ip-api.com/json/{ip}').json()
      
        
        data = {
            '[IP]': response.get('query'),
            '[Int prov]': response.get('isp'),
            '[Org]': response.get('org'),
            '[Country]': response.get('country'),
            '[Region Name]': response.get('regionName'),
            '[City]': response.get('city'),
            '[ZIP]': response.get('zip'),
            '[Lat]': response.get('lat'),
            '[Lon]': response.get('lon'),
        }
        
        for k, v in data.items():
            print(f'{k} : {v}')
        
        area = folium.Map(location=[response.get('lat'), response.get('lon')]) # creation de la map
        folium.Marker([response.get('lat'), response.get('lon')]).add_to(area) # pour cree un marker
        area.save(f'{response.get("query")}_{response.get("city")}.html') # pour enregistrer le fichier html avec le ip addrese et nom de la ville
        
    except requests.exceptions.ConnectionError:
        print('[!] Vérifier votre connexion!')
        
        
def main():
    preview_text = Figlet(font='slant')
    print(preview_text.renderText('IP INFO'))
    ip = input('Veuillez saisir une adresse IP  ')
    
    get_info_by_ip(ip=ip)
    
    
if __name__ == '__main__':
    main()